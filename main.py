from time import sleep

import simpy

# Generador
a = 0
def semaforo(env):
    global  a
    while True:
        a = a + 1
        print("SEM --- ROJO --- {0}". format(str(env.now)))
        d_rojo = 5
        yield env.timeout(d_rojo)

        print("SEM -- AMARRILLO -- %d" %env.now)
        d_amarrillo = 5
        yield env.timeout(d_amarrillo)

        print("SEM - VERDE - {0}".format(str(env.now)))
        d_verde = 2
        yield env.timeout(d_verde)

        print(a)
        sleep(1) # Agregandole una pausa de 10 segundo por proceso

if __name__ == '__main__':
    env = simpy.Environment()
    env.process(semaforo(env))
    env.run(until=720)
    print("Fin = ",a)
